<?php

include 'Qencrypt.php';
use Qencrypt;

class Qdecryptor extends Qencrypt{
	private $string_encripted;
	private $key_decryptor;
	function __construct($string){
		$this->string_encripted = hex2bin($string);
	}

	public function dekrip(){
		$dekripted_text  = array();
		$encryption_text = parent::TabelPeriodikEnq($this->string_encripted);
		// print_r($encryption_text);
		// exit();

		// ALGORITMA RSA
		$hasil=[];
		foreach ($encryption_text as $key => $value) {
		  $pangkat=pow($value,17);
		  $mod=($pangkat % 21);
		  $hasil[$key]=$mod;
		}

		//GABUNG MENJADI ARRAY DUA-DUA
		$odd = array();
		$even = array();
		foreach ($hasil as $k => $v) {
			if ($k % 2 == 0) {
				$even[] = $v;
			}
			else {
				$odd[] = $v;
			}
		}
		$gabung = array();
		foreach ($odd as $key => $value) {
			$gabung[]=$odd[$key].$even[$key];
		}

		// print_r($gabung);
		// exit();

		//MERUBAH ASCII TO DECIMAL
		$dec = "";
		foreach ($gabung as $key => $value) {
			# code...
			$dec[$key] = chr($value);
		}

		$char = parent::AssignCharToNumberEn($dec);
	    // print_r($char);
	    // exit();

		//MERUBAH KE HILL
		//ENKRIPSI DENGAN KUNCI YANG KEDUA
	    $kunci2 =[];
		$kunci2[0][0] = 5;
		$kunci2[0][1] = 6;
		$kunci2[1][0] = 6;
		$kunci2[1][1] = 17;

		$pecah3 = array_chunk($char, 2);

		$level_2 = array();
	    $hill2=[];
	    $hasil2 = $this->perkalian_matriks($pecah3, $kunci2);

	    foreach ($hasil2 as $key => $value) {
	      foreach ($value as $key => $data) {
	        // echo $data;
	        array_push($hill2,($data%26));
	      }
	    }
	    //end hill 2
	    // ENKRIPSI DENGAN KUNCI KE 1
		$kunci =[];
		$kunci[0][0] = 1;
		$kunci[0][1] = 25;
		$kunci[1][0] = 25;
		$kunci[1][1] = 10;
		$pecah2 = array_chunk($hill2, 2);

		// print_r($pecah1);
		//    echo "<pre>";
		// print_r($pecah2);
		// echo "</pre>";

		$hasil = $this->perkalian_matriks($pecah2, $kunci);
	    $hill=[];
	    foreach ($hasil as $key => $value) {
	      foreach ($value as $key => $data) {
	        // echo $data;
	        array_push($hill,($data%26));
	      }
	    }

$plain_array = parent::AssignNumberToChar($hill);
return parent::Finalization($plain_array);
}
}

?>