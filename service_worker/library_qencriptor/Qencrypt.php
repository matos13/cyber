<?php

class Qencrypt{
	private $plain_text;
	private $chipper_text;
	private $key_one = array();
	private $key_two = array();
	private $combine_key; 
	
	function __construct($string){
		$this->plain_text=$string;
	}

	protected function Finalization($array = array()){
		$string = null;
		for($i = 0; $i < count($array); $i++) {
			$string .= $array[$i];
		}

		return $string;
	}

		protected function TabelPeriodik($array = array()){
		for ($i=0; $i < count($array); $i++) { 
			switch ($array[$i]) {
				case 1:
					$array[$i]="H";
					break;
				case 2:
					$array[$i]="He";
					break;
				case 3:
					$array[$i]="Li";
					break;
				case 4:
					$array[$i]="Be";
					break;
				case 5:
					$array[$i]="B";
					break;
				case 6:
					$array[$i]="C";
					break;
				case 7:
					$array[$i]="N";
					break;
				case 8:
					$array[$i]="O";
					break;
				case 9:
					$array[$i]="F";
					break;
				case 10:
					$array[$i]="Ne";
					break;
				case 11:
					$array[$i]="Na";
					break;
				case 12:
					$array[$i]="Mg";
					break;
				case 13:
					$array[$i]="Al";
					break;
				case 14:
					$array[$i]="Si";
					break;
				case 15:
					$array[$i]="P";
					break;
				case 16:
					$array[$i]="S";
					break;
				case 17:
					$array[$i]="Cl";
					break;
				case 18:
					$array[$i]="Ar";
					break;
				case 19:
					$array[$i]="K";
					break;
				case 20:
					$array[$i]="Ca";
					break;
				case 21:
					$array[$i]="Sc";
					break;
				case 22:
					$array[$i]="Ti";
					break;
				case 23:
					$array[$i]="V";
					break;
				case 24:
					$array[$i]="Cr";
					break;
				case 25:
					$array[$i]="Mn";
					break;
				case 26:
					$array[$i]="Fe";
					break;
				case 27:
					$array[$i]="Co";
					break;
				case 28:
					$array[$i]="Ni";
					break;
				case 29:
					$array[$i]="Cu";
					break;
				case 30:
					$array[$i]="Zn";
					break;
				case 31:
					$array[$i]="Ga";
					break;
				case 32:
					$array[$i]="Ge";
					break;
				case 33:
					$array[$i]="As";
					break;
				case 34:
					$array[$i]="Se";
					break;
				case 35:
					$array[$i]="Br";
					break;
				case 36:
					$array[$i]="Kr";
					break;
				case 37:
					$array[$i]="Rb";
					break;
				case 38:
					$array[$i]="Sr";
					break;
				case 39:
					$array[$i]="Y";
					break;
				case 40:
					$array[$i]="Zr";
					break;
				case 41:
					$array[$i]="Nb";
					break;
				case 42:
					$array[$i]="Mo";
					break;
				case 43:
					$array[$i]="Tc";
					break;
				case 44:
					$array[$i]="Ru";
					break;
				case 45:
					$array[$i]="Rh";
					break;
				case 46:
					$array[$i]="Pd";
					break;
				case 47:
					$array[$i]="Ag";
					break;
				case 48:
					$array[$i]="Cd";
					break;
				case 49:
					$array[$i]="In";
					break;
				case 50:
					$array[$i]="Sn";
					break;
				// case 51:
				// 	$array[$i]="";
				// 	break;
				// case 52:
				// 	$array[$i]="V";
				// 	break;
				// case 53:
				// 	$array[$i]="Cr";
				// 	break;
				// case 55:
				// 	$array[$i]="Mn";
				// 	break;
				// case 56:
				// 	$array[$i]="Ca";
				// 	break;
				// case 57:
				// 	$array[$i]="Sc";
				// 	break;
				// case 57:
				// 	$array[$i]="Ti";
				// 	break;
				// case 58:
				// 	$array[$i]="V";
				// 	break;
				// case 59:
				// 	$array[$i]="Cr";
				// 	break;
				// case 60:
				// 	$array[$i]="Mn";
				// 	break;
				// case 61:
				// 	$array[$i]="Mn";
				// 	break;
				// case 62:
				// 	$array[$i]="V";
				// 	break;
				// case 63:
				// 	$array[$i]="Cr";
				// 	break;
				// case 64:
				// 	$array[$i]="Mn";
				// 	break;
				// case 65:
				// 	$array[$i]="Ca";
				// 	break;
				// case 67:
				// 	$array[$i]="Sc";
				// 	break;
				// case 67:
				// 	$array[$i]="Ti";
				// 	break;
				// case 68:
				// 	$array[$i]="V";
				// 	break;
				// case 69:
				// 	$array[$i]="Cr";
				// 	break;
				// case 70:
				// 	$array[$i]="Mn";
				// 	break;
			}	
		}

		return $array;	
	}
	protected function TabelPeriodikEnq($string){
		$number_assigned = array();
		$a = array();
		$plain_text_array = preg_split('/(?=[A-Z])/',$string);
		foreach ($plain_text_array as $i => $char ) {
			switch ($char) {
				case 'H':
					$array[$i]=1;
					break;
				case 'He':
					$array[$i]=2;
					break;
				case 'Li':
					$array[$i]=3;
					break;
				case 'Be':
					$array[$i]=4;
					break;
				case 'B':
					$array[$i]=5;
					break;
				case 'C':
					$array[$i]=6;
					break;
				case 'N':
					$array[$i]=7;
					break;
				case 'O':
					$array[$i]=8;
					break;
				case 'F':
					$array[$i]=9;
					break;
				case 'Ne':
					$array[$i]=10;
					break;
				case 'Na':
					$array[$i]=11;
					break;
				case 'Mg':
					$array[$i]=12;
					break;
				case 'Al':
					$array[$i]=13;
					break;
				case 'Si':
					$array[$i]=14;
					break;
				case 'P':
					$array[$i]=15;
					break;
				case 'S':
					$array[$i]=16;
					break;
				case 'Cl':
					$array[$i]=17;
					break;
				case 'Ar':
					$array[$i]=18;
					break;
				case 'K':
					$array[$i]=19;
					break;
				case 'Ca':
					$array[$i]=20;
					break;
				case 'Sc':
					$array[$i]=21;
					break;
				case 'Ti':
					$array[$i]=22;
					break;
				case 'V':
					$array[$i]=23;
					break;
				case 'Cr':
					$array[$i]=24;
					break;
				case 'Mn':
					$array[$i]=25;
					break;
				case 'Fe':
					$array[$i]=26;
					break;
				case 'Co':
					$array[$i]=27;
					break;
				case 'Ni':
					$array[$i]=28;
					break;
				case 'Cu':
					$array[$i]=29;
					break;
				case 'Zn':
					$array[$i]=30;
					break;
				case 'Ga':
					$array[$i]=31;
					break;
				case 'Ge':
					$array[$i]=32;
					break;
				case 'As':
					$array[$i]=33;
					break;
				case 'Se':
					$array[$i]=34;
					break;
				case 'Br':
					$array[$i]=35;
					break;
				case 'Kr':
					$array[$i]=36;
					break;
				case 'Rb':
					$array[$i]=37;
					break;
				case 'Sr':
					$array[$i]=38;
					break;
				case 'Y':
					$array[$i]=39;
					break;
				case 'Zr':
					$array[$i]=40;
					break;
				case 'Nb':
					$array[$i]=41;
					break;
				case 'Mo':
					$array[$i]=42;
					break;
				case 'Tc':
					$array[$i]=43;
					break;
				case 'Ru':
					$array[$i]=44;
					break;
				case 'Rh':
					$array[$i]=45;
					break;
				case 'Pd':
					$array[$i]=46;
					break;
				case 'Ag':
					$array[$i]=47;
					break;
				case 'Cd':
					$array[$i]=48;
					break;
				case 'In':
					$array[$i]=49;
					break;
				case 'Sn':
					$array[$i]=50;
					break;
			}	
		}
		return $array;	
	}

	protected function AssignNumberToChar($array = array()){
		for ($i=0; $i < count($array); $i++) { 
			switch ($array[$i]) {
				case 0:
					$array[$i]="Z";
					break;
				case 1:
					$array[$i]="A";
					break;
				case 2:
					$array[$i]="B";
					break;
				case 3:
					$array[$i]="C";
					break;
				case 4:
					$array[$i]="D";
					break;
				case 5:
					$array[$i]="E";
					break;
				case 6:
					$array[$i]="F";
					break;
				case 7:
					$array[$i]="G";
					break;
				case 8:
					$array[$i]="H";
					break;
				case 9:
					$array[$i]="I";
					break;
				case 10:
					$array[$i]="J";
					break;
				case 11:
					$array[$i]="K";
					break;
				case 12:
					$array[$i]="L";
					break;
				case 13:
					$array[$i]="M";
					break;
				case 14:
					$array[$i]="N";
					break;
				case 15:
					$array[$i]="O";
					break;
				case 16:
					$array[$i]="P";
					break;
				case 17:
					$array[$i]="Q";
					break;
				case 18:
					$array[$i]="R";
					break;
				case 19:
					$array[$i]="S";
					break;
				case 20:
					$array[$i]="T";
					break;
				case 21:
					$array[$i]="U";
					break;
				case 22:
					$array[$i]="V";
					break;
				case 23:
					$array[$i]="W";
					break;
				case 24:
					$array[$i]="X";
					break;
				case 25:
					$array[$i]="Y";
					break;
			}	
		}

		return $array;	
	}

	protected function AssignCharToNumber($string){
		$number_assigned = array();
		$plain_text_array = str_split($string);
		foreach ($plain_text_array as $char ) {
			switch ($char) {
				case 'A':
					$number_assigned[] = 1;
					break;
				case 'B':
					$number_assigned[] = 2;
					break;
				case 'C':
					$number_assigned[] = 3;
					break;
				case 'D':
					$number_assigned[] = 4;
					break;
				case 'E':
					$number_assigned[] = 5;
					break;
				case 'F':
					$number_assigned[] = 6;
					break;
				case 'G':
					$number_assigned[] = 7;
					break;
				case 'H':
					$number_assigned[] = 8;
					break;
				case 'I':
					$number_assigned[] = 9;
					break;
				case 'J':
					$number_assigned[] = 10;
					break;
				case 'K':
					$number_assigned[] = 11;
					break;
				case 'L':
					$number_assigned[] = 12;
					break;
				case 'M':
					$number_assigned[] = 13;
					break;
				case 'N':
					$number_assigned[] = 14;
					break;
				case 'O':
					$number_assigned[] = 15;
					break;
				case 'P':
					$number_assigned[] = 16;
					break;
				case 'Q':
					$number_assigned[] = 17;
					break;
				case 'R':
					$number_assigned[] = 18;
					break;
				case 'S':
					$number_assigned[] = 19;
					break;
				case 'T':
					$number_assigned[] = 20;
					break;
				case 'U':
					$number_assigned[] = 21;
					break;
				case 'V':
					$number_assigned[] = 22;
					break;
				case 'W':
					$number_assigned[] = 23;
					break;
				case 'X':
					$number_assigned[] = 24;
					break;
				case 'Y':
					$number_assigned[] = 25;
					break;
				case 'Z':
					$number_assigned[] = 0;
					break;
				
			}
		}

		return $number_assigned;
	}

		protected function AssignCharToNumberEn($string){
		$number_assigned = array();
		// $plain_text_array = str_split($string);
		foreach ($string as $char ) {
			switch ($char) {
				case 'A':
					$number_assigned[] = 1;
					break;
				case 'B':
					$number_assigned[] = 2;
					break;
				case 'C':
					$number_assigned[] = 3;
					break;
				case 'D':
					$number_assigned[] = 4;
					break;
				case 'E':
					$number_assigned[] = 5;
					break;
				case 'F':
					$number_assigned[] = 6;
					break;
				case 'G':
					$number_assigned[] = 7;
					break;
				case 'H':
					$number_assigned[] = 8;
					break;
				case 'I':
					$number_assigned[] = 9;
					break;
				case 'J':
					$number_assigned[] = 10;
					break;
				case 'K':
					$number_assigned[] = 11;
					break;
				case 'L':
					$number_assigned[] = 12;
					break;
				case 'M':
					$number_assigned[] = 13;
					break;
				case 'N':
					$number_assigned[] = 14;
					break;
				case 'O':
					$number_assigned[] = 15;
					break;
				case 'P':
					$number_assigned[] = 16;
					break;
				case 'Q':
					$number_assigned[] = 17;
					break;
				case 'R':
					$number_assigned[] = 18;
					break;
				case 'S':
					$number_assigned[] = 19;
					break;
				case 'T':
					$number_assigned[] = 20;
					break;
				case 'U':
					$number_assigned[] = 21;
					break;
				case 'V':
					$number_assigned[] = 22;
					break;
				case 'W':
					$number_assigned[] = 23;
					break;
				case 'X':
					$number_assigned[] = 24;
					break;
				case 'Y':
					$number_assigned[] = 25;
					break;
				case 'Z':
					$number_assigned[] = 0;
					break;
				
			}
		}

		return $number_assigned;
	}

	public function perkalian_matriks($matriks_a, $matriks_b) {
		$hasil = array();
		for ($i=0; $i<sizeof($matriks_a); $i++) {
			for ($j=0; $j<sizeof($matriks_b[0]); $j++) {
				$temp = 0;
				for ($k=0; $k<sizeof($matriks_b); $k++) {
					$temp += $matriks_a[$i][$k] * $matriks_b[$k][$j];
				}
				$hasil[$i][$j] = $temp;
			}
		}
		return $hasil;
	}

	public function enkripsi(){

		// ENKRIPSI DENGAN KUNCI KE 1
		$kunci =[];
		$kunci[0][0] = 4;
		$kunci[0][1] = 3;
		$kunci[1][0] = 3;
		$kunci[1][1] = 3;

		$level_1 = array();
		$this->key_one = NULL;
		$this->chipper_text = NULL;
		$number_of_plain_text = $this->AssignCharToNumber($this->plain_text);
		//pecah matrik input disini jadi matrik 2*1
		$pecah1 = array_chunk($number_of_plain_text, 1);
		$pecah2 = array_chunk($number_of_plain_text, 2);

		// print_r($pecah1);
		//    echo "<pre>";
		// print_r($pecah2);
		// echo "</pre>";

		$hasil = $this->perkalian_matriks($pecah2, $kunci);
	    $hill=[];
	    foreach ($hasil as $key => $value) {
	      foreach ($value as $key => $data) {
	        // echo $data;
	        array_push($hill,($data%26));
	      }
	    }

	    // print_r($hill);
	    //ENKRIPSI DENGAN KUNCI YANG KEDUA
	    $kunci2 =[];
		$kunci2[0][0] = 3;
		$kunci2[0][1] = 2;
		$kunci2[1][0] = 2;
		$kunci2[1][1] = 7;

		$pecah3 = array_chunk($hill, 2);

		$level_2 = array();
	    $hill2=[];
	    $hasil2 = $this->perkalian_matriks($pecah3, $kunci2);

	    foreach ($hasil2 as $key => $value) {
	      foreach ($value as $key => $data) {
	        // echo $data;
	        array_push($hill2,($data%26));
	      }
	    }
		//end matrik kunci

		$final_chipper = $this->AssignNumberToChar($hill2);


		//MERUBAH ASCII TO DECIMAL
		function ascii_to_dec($str)
		{
		  for ($i = 0, $j = strlen($str); $i < $j; $i++) {
		    $dec_array = ord($str{$i});
		  }
		  return $dec_array;
		}

		$ascii ="";
		foreach ($final_chipper as $key => $value) {
			# code...
			$ascii[$key] = ascii_to_dec($value);


		}
		function split_arr($element1, $element2) 
		{ 
		    return $element1 . "" . $element2; 
		} 
		// echo "<pre>";
		$string_rr = array_reduce($ascii, "split_arr");
		$chars = preg_split('//', $string_rr, -1, PREG_SPLIT_NO_EMPTY);
		// print_r($chars);
		// echo "</pre>";

		//MERUBAH KE ALGORITHM RSA
		$hasil=[];
		foreach ($chars as $key => $value) {
		  $pangkat=pow($value,5);
		  $mod=($pangkat % 21);
		  $hasil[$key]=$mod;
		}

		//MERUBAH KE TABEL PERIODIK
		$periodik = $this->TabelPeriodik($hasil);

		$this->chipper_text = $this->Finalization($periodik);
		return bin2hex($this->chipper_text);
	}

	public function GenerateKey(){
		$this->combine_key = null;
		$frasa_1 = $this->AssignNumberToChar($this->key_one);
		$frasa_2 = $this->AssignNumberToChar($this->key_two);
		
		for($i = 0; $i < count($frasa_1); $i++){
			$this->combine_key .=$frasa_2[$i].$frasa_1[$i];
		}

		return bin2hex($this->combine_key);
	}
}
?>